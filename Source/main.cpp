#include "SDL.h"
#include "SDL_image.h"
//#include "ResourcePath.h"

#if defined(__IPHONEOS__) || defined(__ANDROID__)
#define HAVE_OPENGLES
#endif

#ifdef HAVE_OPENGLES
#include "SDL_opengles.h"
#else
#include "SDL_opengl.h"
#endif

#include <memory>
using namespace std;

SDL_Surface * TextureImage[1];
GLuint texture[1]; /* Storage For One Texture ( NEW ) */

static void __gluMakeIdentityf(GLfloat m[16])
{
    m[0+4*0] = 1; m[0+4*1] = 0; m[0+4*2] = 0; m[0+4*3] = 0;
    m[1+4*0] = 0; m[1+4*1] = 1; m[1+4*2] = 0; m[1+4*3] = 0;
    m[2+4*0] = 0; m[2+4*1] = 0; m[2+4*2] = 1; m[2+4*3] = 0;
    m[3+4*0] = 0; m[3+4*1] = 0; m[3+4*2] = 0; m[3+4*3] = 1;
}

void gluPerspective(GLfloat fovy, GLfloat aspect, GLfloat zNear, GLfloat zFar)
{
    GLfloat m[4][4];
    GLfloat sine, cotangent, deltaZ;
    GLfloat radians = fovy / 2 * 3.14 / 180;
    
    deltaZ = zFar - zNear;
    sine = sin(radians);
    if ((deltaZ == 0) || (sine == 0) || (aspect == 0))
    {
        return;
    }
    cotangent = cos(radians) / sine;
    
    __gluMakeIdentityf(&m[0][0]);
    m[0][0] = cotangent / aspect;
    m[1][1] = cotangent;
    m[2][2] = -(zFar + zNear) / deltaZ;
    m[2][3] = -1;
    m[3][2] = -2 * zNear * zFar / deltaZ;
    m[3][3] = 0;
    glMultMatrixf(&m[0][0]);
}


const GLfloat verts[] = {
    -1.0f, -1.0f,  0.0f,
    -1.0f,  1.0f,  0.0f,
    1.0f,  1.0f,  0.0f,
    1.0f, -1.0f,  0.0f,
    0.0f,  1.0f, 0.0f
};

const GLfloat quadVerts[] = {
//    1.0f,1.0f,1.0,-1.0,1.0,			// 2
//    0.0f,1.0f,1.0,1.0,1.0,			// 6
//    1.0f,0.0f,-1.0,-1.0,1.0,		// 3
//    0.0f,0.0f,-1.0,1.0,1.0,			// 7
    0.0f,1.0f, -1.0,-1.0,1.0,			// 2
    0.0f,0.0f, -1.0,1.0,1.0,			// 6
    1.0f,1.0f, 1.0,-1.0,1.0,		// 3
    1.0f,0.0f, 1.0,1.0,1.0			// 7
};


const GLubyte quadIdx[] = {
    0, 1, 2, 3
};

const GLubyte triIdx[] = {
    0, 4, 3
};

class Graphics
{
private:
    
    SDL_Window* _window;
    GLfloat     rtri;                       // Angle For The Triangle ( NEW )
    GLfloat     rquad;                       // Angle For The Quad ( NEW )
    
public:
    
    Graphics(SDL_Window* window)
    {
        _window = window;
        rtri = 0.0f;
        rquad = 0.0f;
    }
    
    void initGL()
    {
        resizeGL();
        
        //TextureImage[0] = SDL_LoadBMP("nehe.bmp");
        TextureImage[0] = IMG_Load("nehe.png");
        
        if ( TextureImage[0] )
        {
//            SDL_Surface* gScreenSurface = SDL_GetWindowSurface( _window );
//            SDL_Surface * optimizedSurface = SDL_ConvertSurface( TextureImage[0], gScreenSurface->format, NULL );
//            if( optimizedSurface == NULL )
//            {
//                SDL_Log( "Unable to optimize image! SDL Error: %s\n", SDL_GetError() );
//            }
//            
//            //Get rid of old loaded surface
//            SDL_FreeSurface( TextureImage[0] );
//            
//            TextureImage[0] = optimizedSurface;
            
            /* Create The Texture */
            glGenTextures( 1, &texture[0] );
            
            /* Typical Texture Generation Using Data From The Bitmap */
            glBindTexture( GL_TEXTURE_2D, texture[0] );
            
            GLenum Format;
            GLint Colors = TextureImage[0]->format->BytesPerPixel;
            
            if (Colors == 4 )
            {
                if (TextureImage[0]->format->Rmask == 0x000000ff)
                    Format = GL_RGBA;
                else
                    Format = GL_BGRA;
            }
            else if (Colors == 3)
            {
                if (TextureImage[0]->format->Rmask == 0x000000ff)
                    Format = GL_RGB;
                else
                {
#ifdef HAVE_OPENGLES
                    Format = GL_RGB;
#else
                    Format = GL_BGR;
#endif
                }
            }
            else
            {
                SDL_Log("Cannot determine format");
            }
            
            /* Generate The Texture */
            glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, TextureImage[0]->w,
                         TextureImage[0]->h, 0, Format,
                         GL_UNSIGNED_BYTE, TextureImage[0]->pixels );
            
            /* Linear Filtering */
            glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
            glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
        }
        else
        {
            SDL_Log("Cannot load Texture");
        }
        
        if ( TextureImage[0] )
            SDL_FreeSurface( TextureImage[0] );
        
        //glOrthof(-2, 2, -2, 2, -2, 2);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        
        glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
        glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
        glShadeModel(GL_SMOOTH);
        
        /* Enable Texture Mapping ( NEW ) */
        glEnable( GL_TEXTURE_2D );
        
        glEnable(GL_DEPTH_TEST);                        // Enables Depth Testing
        glDepthFunc(GL_LEQUAL);                         // The Type Of Depth Test To Do
        
#if defined(HAVE_OPENGLES)
        glClearDepthf(1.0f);                            // Depth Buffer Setup
#else
        glClearDepth(1.0f);                            // Depth Buffer Setup
#endif

    }
    
    void resizeGL()
    {
        int width, height;
        
        SDL_GetWindowSize(_window, &width, &height);
        
        glViewport(0, 0, width, height);
        
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        
        gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);
    }
    
    static int EventFilter(void* userdata, SDL_Event* event)
    {
        Graphics* graphics = (Graphics*)userdata;
        
        switch (event->type)
        {
            case SDL_WINDOWEVENT:
            {
                switch (event->window.event)
                {
                    case SDL_WINDOWEVENT_RESIZED:
                    {
                        SDL_Log("Window %d resized to %dx%d", event->window.windowID, event->window.data1, event->window.data2);
                        
                        graphics->resizeGL();
                        
                        break;
                    }
                }
            }
        }
        
        return 1;
    }
    
    void update()
    {
        /* These are to calculate our fps */
        static GLint T0     = 0;
        static GLint Frames = 0;
        
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        glEnableClientState(GL_VERTEX_ARRAY);
        
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        
        /* Render Triangle */
        glVertexPointer(3, GL_FLOAT, 0, verts);
        glBindTexture( GL_TEXTURE_2D, NULL );
        glTranslatef(-1.5f,0.0f,-6.0f);             // Move Into The Screen And Left
        
        glRotatef(rtri, 0.0f, 1.0f, 0.0f);
        
        glColor4f(1.0f, 0.0f, 1.0f, 1.0f);
        glDrawElements(GL_TRIANGLES, 3, GL_UNSIGNED_BYTE, triIdx);
        
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        
        /* Render Quad */
        glBindTexture( GL_TEXTURE_2D, texture[0] );
        glTranslatef(1.5f,0.0f,-6.0f);             // Move Into The Screen And Right
        
        glRotatef(rquad, 0.0f, 0.0f, -1.0f);
        
        glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        
        glTexCoordPointer(2, GL_FLOAT, 5 * sizeof(GL_FLOAT), quadVerts);
        glVertexPointer(3, GL_FLOAT, 5 * sizeof(GL_FLOAT), quadVerts + 2);
        
        // Initial setup
        glEnableClientState(GL_VERTEX_ARRAY);
        glEnableClientState(GL_TEXTURE_COORD_ARRAY);
        
        // Enable transparency for texture
        glEnable (GL_BLEND);
        glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        
        glDrawElements(GL_TRIANGLE_STRIP, 4, GL_UNSIGNED_BYTE, quadIdx);
        
        
        SDL_GL_SwapWindow(_window);
        
        /* Gather our frames per second */
        Frames++;
        {
            GLint t = SDL_GetTicks();
            if (t - T0 >= 5000) {
                GLfloat seconds = (t - T0) / 1000.0;
                GLfloat fps = Frames / seconds;
                SDL_Log("%d frames in %g seconds = %g FPS\n", Frames, seconds, fps);
                T0 = t;
                Frames = 0;
            }
        }
        
        rtri+=0.6f;                     // Increase The Rotation Variable For The Triangle ( NEW )
        rquad+=0.3f;                     // Increase The Rotation Variable For The Triangle ( NEW )
    }
};

void UpdateFrame(void* param)
{
    Graphics* graphics = (Graphics*)param;
    graphics->update();
}

int EventFilter(void* userdata, SDL_Event* event)
{
    switch (event->type)
    {
        case SDL_FINGERMOTION:
            SDL_Log("Finger Motion");
            return 0;
            
        case SDL_FINGERDOWN:
            SDL_Log("Finger Down");
            return 0;
            
        case SDL_FINGERUP:
            SDL_Log("Finger Up");
            return 0;
            
        case SDL_KEYDOWN:
            return 0;
            
        case SDL_KEYUP:
            return 0;
            
        case SDL_MOUSEBUTTONDOWN:
            return 0;
            
        case SDL_MOUSEBUTTONUP:
            return 0;
            
        case SDL_MOUSEWHEEL:
            return 0;
            
        case SDL_MOUSEMOTION:
            return 0;
    }
    
    return 1;
}

int main(int argc, char *argv[])
{
    /* initialize SDL */
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        printf("Could not initialize SDL\n");
        return 1;
    }
    
    SDL_DisplayMode displayMode;
    SDL_GetDesktopDisplayMode(0, &displayMode);
    
    int width = 640;
    int height = 480;
    Uint32 flags = SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE;

#if defined(__IPHONEOS__)
    width = displayMode.w;
    height = displayMode.h;
    flags |= SDL_WINDOW_FULLSCREEN;
#endif
    
    /* create window and renderer */
    SDL_Window* window = SDL_CreateWindow("SDL 2 Window", 0, 0, width, height, flags);
    
    if (!window) {
        printf("Could not initialize Window\n");
        return 1;
    }
    
    //Initialize PNG loading
    int imgFlags = IMG_INIT_PNG;
    if( !( IMG_Init( imgFlags ) & imgFlags ) )
    {
        printf( "SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError() );
        //success = false;
    }
    else
    {
        //Get window surface
        //gScreenSurface = SDL_GetWindowSurface( gWindow );
    }
    
    auto gl = SDL_GL_CreateContext(window);
    
    unique_ptr<Graphics> graphics = unique_ptr<Graphics>(new Graphics(window));

#if defined(__IPHONEOS__)
    SDL_iPhoneSetAnimationCallback(window, 1, UpdateFrame, graphics.get());
#endif
    
    SDL_AddEventWatch(EventFilter, NULL);
    SDL_AddEventWatch(graphics->EventFilter, graphics.get());
    
    graphics->initGL();
    
    //Game Loop
    SDL_Event event;
    auto done = false;
    while (!done)
    {
        SDL_PumpEvents();
        while (SDL_PollEvent(&event))
        {
            switch (event.type)
            {
                case SDL_QUIT:
                    done = true;
                    break;
                    
                case SDL_APP_DIDENTERFOREGROUND:
                    SDL_Log("SDL_APP_DIDENTERFOREGROUND");
                    break;
                    
                case SDL_APP_DIDENTERBACKGROUND:
                    SDL_Log("SDL_APP_DIDENTERBACKGROUND");
                    break;
                    
                case SDL_APP_LOWMEMORY:
                    SDL_Log("SDL_APP_LOWMEMORY");
                    break;
                    
                case SDL_APP_TERMINATING:
                    SDL_Log("SDL_APP_TERMINATING");
                    break;
                    
                case SDL_APP_WILLENTERBACKGROUND:
                    SDL_Log("SDL_APP_WILLENTERBACKGROUND");
                    break;
                    
                case SDL_APP_WILLENTERFOREGROUND:
                    SDL_Log("SDL_APP_WILLENTERFOREGROUND");
                    break;
                    
//                case SDL_WINDOWEVENT:
//                {
//                    switch (event.window.event)
//                    {
//                        case SDL_WINDOWEVENT_RESIZED:
//                        {
//                            SDL_Log("Window %d resized to %dx%d", event.window.windowID, event.window.data1, event.window.data2);
//                            
//                            graphics->resizeGL();
//                            
//                            break;
//                        }
//                    }
//                }
            }
        }
        
#if !defined(__IPHONEOS__)
        UpdateFrame(graphics.get());
#endif
    }
    
    SDL_GL_DeleteContext(gl);
    
    // Done! Close the window, clean-up and exit the program.
    SDL_DestroyWindow(window);
    SDL_Quit();
    
    return 0;
}
